import React, {useState, useEffect} from "react";

function Aufgabe_2() {

    const [todos, setTodos] = useState([])
    const [userId, setUserId] = useState(1)

    useEffect( () => {
        fetchData()
    },[]);

    async function fetchData() {
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodos(data);
            })
    }


    return (
        <ul>
            { todos.map(todo => <li key={todo.id}>{todo.title}</li> )}
        </ul>
    );
}

export default Aufgabe_2;
